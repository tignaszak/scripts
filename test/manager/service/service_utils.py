from manager.service.service.model.service import Service


def get_service(name: str) -> Service:
    return Service(name, '', '')


CLOUD_CONFIG: Service = get_service('Cloud-config')
EUREKA: Service = get_service('Eureka')
GATEWAY: Service = get_service('Gateway')
MESSAGES: Service = get_service('V1-Messages')
TASKS: Service = get_service('V1-Tasks')
TOKENS: Service = get_service('V1-Tokens')
USERS: Service = get_service('V1-Users')
