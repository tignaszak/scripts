import pathlib
import unittest
from os import path
from typing import Set, Dict, Tuple

from manager.service.dependency.reader.reader import Reader
from manager.service.dependency.reader.reader_interface import ReaderInterface
from manager.service.dependency.repository.dependency import Dependency
from manager.service.dependency.repository.dependency_repository_interface import DependencyRepositoryInterface
from manager.service.project.model.project import Project
from manager.service.service.model.service import Service
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface

TEST_SERVICES_DIR: str = path.join(str(pathlib.Path(__file__).parent.resolve()), 'services')

CLOUD_CONFIG_SERVICE = Service('Cloud-config', TEST_SERVICES_DIR, '')
EUREKA_SERVICE = Service('Eureka', TEST_SERVICES_DIR, '')
MESSAGES_SERVICE = Service('Messages', TEST_SERVICES_DIR, '')
TOKENS_SERVICE = Service('Tokens', TEST_SERVICES_DIR, '')
USERS_SERVICE = Service('Users', TEST_SERVICES_DIR, '')


class DependencyRepositoryMock(DependencyRepositoryInterface):

    def __init__(self, outer):
        self.__outer = outer

    def add(self, service: Service, required: Set[Service], optional=None):
        if optional is None:
            optional = set()
        self.__outer.dependencies[service] = Dependency(required, optional)
        return self

    def find_dependency_by_service(self, service: Service) -> Dependency:
        pass

    def find_service_to_dependency_map(self) -> Dict[Service, Dependency]:
        pass

    def contains_service(self, service: Service) -> bool:
        pass


class ProjectRepositoryMock(ProjectRepositoryInterface):
    def find_all_projects(self) -> Tuple[Project]:
        pass

    def find_all_services(self) -> Tuple[Service]:
        return (
            EUREKA_SERVICE,
            TOKENS_SERVICE,
            CLOUD_CONFIG_SERVICE,
            MESSAGES_SERVICE
        )

    def find_selected_services(self) -> Tuple[Service]:
        pass

    def find_service_by_name(self, name: str) -> Service:
        for service in self.find_all_services():
            if name == service.get_name():
                return service
        return None


class TestReader(unittest.TestCase):

    dependencies: Dict[Service, Dependency]
    __subject: ReaderInterface

    def setUp(self) -> None:
        self.dependencies = {}
        self.__subject = Reader(DependencyRepositoryMock(self), ProjectRepositoryMock())

    def test_load_services_from_build_file(self):
        # given
        service = USERS_SERVICE

        # when
        self.__subject.read(service)

        # then
        self.assertEqual(5, len(self.dependencies))
        self.assertIn(EUREKA_SERVICE, self.dependencies)
        self.assertIn(CLOUD_CONFIG_SERVICE, self.dependencies)
        self.assertIn(TOKENS_SERVICE, self.dependencies)
        self.assertIn(MESSAGES_SERVICE, self.dependencies)
        self.assertIn(USERS_SERVICE, self.dependencies)


if __name__ == '__main__':
    unittest.main()
