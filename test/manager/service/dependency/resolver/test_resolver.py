import unittest
from typing import Set, Dict

from manager.service.dependency.repository.dependency import Dependency
from manager.service.dependency.repository.dependency_repository_interface import DependencyRepositoryInterface
from manager.service.dependency.resolver.resolver import Resolver
from manager.service.dependency.resolver.resolver_interface import ResolverInterface
from manager.service.service.model.service import Service
from test.manager.service.service_utils import GATEWAY, USERS, TOKENS, MESSAGES, TASKS, EUREKA, CLOUD_CONFIG


class TestResolver(unittest.TestCase):

    class RepositoryMock(DependencyRepositoryInterface):
        def add(self, service: Service, required: Set[Service], optional=None):
            pass

        def find_service_to_dependency_map(self) -> Dict[Service, Dependency]:
            return {
                GATEWAY: Dependency({EUREKA, CLOUD_CONFIG}, {TOKENS}),
                USERS: Dependency({EUREKA, CLOUD_CONFIG, GATEWAY}, {MESSAGES, TOKENS}),
                TOKENS: Dependency({EUREKA, CLOUD_CONFIG, GATEWAY}, set()),
                MESSAGES: Dependency({EUREKA, CLOUD_CONFIG}, set()),
                TASKS: Dependency({EUREKA, CLOUD_CONFIG, GATEWAY}, {TOKENS})
            }

        def find_dependency_by_service(self, service: Service) -> Dependency:
            return self.find_service_to_dependency_map()[service]

        def contains_service(self, service: Service) -> bool:
            return service in self.find_service_to_dependency_map()

    resolver: ResolverInterface = Resolver(RepositoryMock())

    def test_resolve_for_single_service_without_optionals(self):
        # when
        dependencies = self.resolver.resolve(tuple([MESSAGES]))

        # then
        expected = tuple([
            tuple([CLOUD_CONFIG, EUREKA])
        ])

        self.assertTupleEqual(expected, dependencies)

    def test_resolve_for_single_service_with_optional(self):
        # when
        dependencies = self.resolver.resolve(tuple([USERS]))

        # then
        expected = (
            (CLOUD_CONFIG, EUREKA),
            tuple([GATEWAY]),
            (MESSAGES, TOKENS)
        )

        self.assertTupleEqual(expected, dependencies)

    def test_resolve_for_many_services(self):
        # when
        dependencies = self.resolver.resolve(tuple([USERS, TASKS]))

        # then
        expected = (
            (CLOUD_CONFIG, EUREKA),
            tuple([GATEWAY]),
            (MESSAGES, TOKENS)
        )

        self.assertTupleEqual(expected, dependencies)

    def test_resolve_for_not_added_service(self):
        # when
        dependencies = self.resolver.resolve(tuple([EUREKA]))

        # then
        expected = tuple()

        self.assertTupleEqual(expected, dependencies)


if __name__ == '__main__':
    unittest.main()
