import pathlib
from os import path

TEST_OUTPUT_DIR: str = path.join(str(pathlib.Path(__file__).parent.resolve()), 'output')


def read_output(file_name: str) -> str:
    file = open(path.join(TEST_OUTPUT_DIR, file_name), "r")
    output = file.read()
    file.close()
    return output
