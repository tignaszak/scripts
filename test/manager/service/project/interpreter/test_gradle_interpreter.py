import unittest

from manager.service.project.interpreter.gradle_interpreter import GradleInterpreter
from manager.service.project.interpreter.result.build_result import BuildStatus
from manager.service.project.interpreter.result.test_result import TestStatus
from test.manager.service.project.interpreter.test_utils import read_output


class TestGradleInterpreter(unittest.TestCase):

    subject = GradleInterpreter()

    def test_successful(self):
        # given
        output = read_output('gradle_test_success.txt')

        # when
        result = self.subject.interpret_test_output(output)

        # then
        self.assertEqual(29, result.total_tests)
        self.assertEqual(29, result.success)
        self.assertEqual(0, result.failures)
        self.assertEqual(0, result.skipped)
        self.assertEqual(TestStatus.SUCCESS, result.get_status())

    def test_failed(self):
        # given
        output = read_output('gradle_test_fail.txt')

        # when
        result = self.subject.interpret_test_output(output)

        # then
        self.assertEqual(29, result.total_tests)
        self.assertEqual(28, result.success)
        self.assertEqual(1, result.failures)
        self.assertEqual(0, result.skipped)
        self.assertEqual(TestStatus.FAILED, result.get_status())

    def test_no_tests(self):
        # when
        result = self.subject.interpret_test_output("")

        # then
        self.assertEqual(0, result.total_tests)
        self.assertEqual(0, result.success)
        self.assertEqual(0, result.failures)
        self.assertEqual(0, result.skipped)
        self.assertEqual(TestStatus.NO_TESTS, result.get_status())

    def test_build_successful(self):
        # given
        output = read_output('gradle_build_success.txt')

        # when
        result = self.subject.interpret_build_output(output)

        # then
        self.assertEqual(BuildStatus.SUCCESS, result.status)

    def test_build_fail(self):
        # when
        result = self.subject.interpret_build_output('error')

        # then
        self.assertEqual(BuildStatus.FAILED, result.status)


if __name__ == '__main__':
    unittest.main()
