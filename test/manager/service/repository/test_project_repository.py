import unittest

from manager.service.project.model.gradle_project import GradleProject
from manager.service.project.model.maven_project import MavenProject
from manager.service.repository.project_repository import ProjectRepository
from manager.service.service.model.service import Service

TASKS_PROJECT = GradleProject(Service('tasks', '', '', True), 'tasks', '', '')
TOKENS_PROJECT = MavenProject(Service('tokens', '', '', True), 'tokens', '', '')
USERS_PROJECT = MavenProject(Service('users', '', ''), 'users', '', '')
MESSAGES_PROJECT = MavenProject(None, 'messages', '', '')


class TestProjectRepository(unittest.TestCase):

    __subject = ProjectRepository(tuple((USERS_PROJECT, TOKENS_PROJECT, TASKS_PROJECT, MESSAGES_PROJECT)))

    def test_find_all_projects(self):
        # when
        result = self.__subject.find_all_projects()

        # then
        self.assertEqual(4, len(result))

    def test_find_all_services(self):
        # when
        result = self.__subject.find_all_services()

        # then
        self.assertEqual(3, len(result))

    def test_find_selected_services(self):
        # when
        result = self.__subject.find_selected_services()

        # then
        self.assertEqual(2, len(result))

    def test_find_service_by_name(self):
        # when
        result = self.__subject.find_service_by_name('users')

        # then
        self.assertEqual('users', result.get_name())


if __name__ == '__main__':
    unittest.main()
