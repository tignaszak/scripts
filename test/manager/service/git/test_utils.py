import pathlib
from os import path

TEST_RESPONSE_DIR: str = path.join(str(pathlib.Path(__file__).parent.resolve()), 'response')


def read_response(file_name: str) -> str:
    file = open(path.join(TEST_RESPONSE_DIR, file_name), "r")
    output = file.read()
    file.close()
    return output
