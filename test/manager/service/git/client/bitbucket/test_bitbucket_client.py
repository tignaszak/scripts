import unittest

from manager.manager_exception import ManagerException
from manager.service.git.client.bitbucket.bitbucket_client import BitbucketClient
from manager.service.git.client.http_client import HttpClient, Response
from test.manager.service.git.test_utils import read_response


class HttpClientMock(HttpClient):

    def __init__(self, code: int, body: str):
        super().__init__('')
        self.__code = code
        self.__body = body

    def get(self, path: str) -> Response:
        return Response(self.__code, self.__body)


class TestBitbucketClient(unittest.TestCase):

    def test_parse_response_to_repositories(self):
        # given
        body = read_response('bitbucket_success_response.json')
        http_client = HttpClientMock(200, body)
        subject = BitbucketClient(http_client)

        # when
        repositories = subject.get_repositories()

        # then
        self.assertEqual(10, len(repositories))

    def test_error_while_calling_resource(self):
        # given
        http_client = HttpClientMock(500, 'Server error')
        subject = BitbucketClient(http_client)

        # when get_repositories is called then raise an exception
        with self.assertRaises(ManagerException):
            subject.get_repositories()


if __name__ == '__main__':
    unittest.main()
