import unittest

from manager.service.service.model.service import Service


class TestService(unittest.TestCase):
    def test_get_main_class_name(self):
        # given
        service = Service('service-name', 'path', 'container_name')

        # when
        class_name = service.get_main_class_name()

        # then
        self.assertEqual('ServiceNameApplication', class_name)


if __name__ == '__main__':
    unittest.main()
