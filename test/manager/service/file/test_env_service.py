import pathlib
import unittest
from os import path

from manager.service.file.env_service import EnvService

TEST_ENV_FILE: str = path.join(str(pathlib.Path(__file__).parent.resolve()), 'env', 'main-dev.env')


class TestEnvService(unittest.TestCase):

    def test_read_default_env_file(self):
        # given
        subject = EnvService(TEST_ENV_FILE, '')

        # when
        result = subject.get_env_vars_string()

        # then
        self.assertEqual(' manager_eureka_port=8761 manager_gateway_port=8762 ', result)

    def test_read_custom_env_file(self):
        # given
        subject = EnvService('/some/default/env/file/path', TEST_ENV_FILE)

        # when
        result = subject.get_env_vars_string()

        # then
        self.assertEqual(' manager_eureka_port=8761 manager_gateway_port=8762 ', result)

    def test_get_env_option_by_name(self):
        # given
        subject = EnvService(TEST_ENV_FILE, '')

        # when
        result = subject.get_env_var_by_name('manager_eureka_port')

        # then
        self.assertEqual('8761', result)

    def test_get_env_option_by_invalid_name(self):
        # given
        subject = EnvService(TEST_ENV_FILE, '')

        # when
        result = subject.get_env_var_by_name('invalid_variable')

        # then
        self.assertEqual(None, result)


if __name__ == '__main__':
    unittest.main()
