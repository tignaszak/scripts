import unittest

from manager.service.docker.image.repository.image_repository import ImageRepository
from manager.service.executor import Executor


class ExecutorMock(Executor):
    def check_exec_shell(self, command: str) -> str:
        return 'REPOSITORY                       TAG            IMAGE ID       CREATED         SIZE'\
            'manager-users-v1-service         latest         d2cda5727888   19 hours ago    409MB'\
            'manager-tasks-v1-service         latest         645aa34b82ba   2 days ago      407MB'\
            'manager-tokens-v1-service        latest         c3a6d7b679f3   2 days ago      390MB'\
            'manager-messages-v1-service      latest         f485ea82cfb8   2 days ago      374MB'\
            'manager-gateway                  latest         407991ea6740   2 days ago      395MB'\
            'manager-cloud-config-server      latest         21a39320a5ad   2 days ago      375MB'\
            'manager-eureka                   latest         c2fb30c15baa   2 days ago      380MB'\
            'rabbitmq                         3-management   aaeff1fdd203   8 days ago      254MB'\
            'rabbitmq                         management     aaeff1fdd203   8 days ago      254MB'\
            'redis                            alpine         3900abf41552   8 weeks ago     32.4MB'\
            'testcontainers/ryuk              0.3.3          64f4b02dc986   3 months ago    12MB'\
            'rediscommander/redis-commander   latest         778af9bd6397   6 months ago    77.8MB'\
            'openjdk                          17-alpine      264c9bdce361   7 months ago    326MB'\
            'postgres                         12.1           cf879a45faaa   24 months ago   394MB'\
            'reachfive/fake-smtp-server       latest         ad831d8fb38f   2 years ago     322MB'


class TestImageRepository(unittest.TestCase):

    def test_find_image_by_name(self):
        # given
        subject = ImageRepository(ExecutorMock(False, False))

        # when
        result = subject.find_image_by_name('manager-messages-v1-service')

        # then
        self.assertIsNotNone(result)
        self.assertEqual('f485ea82cfb8', result.get_id())


if __name__ == '__main__':
    unittest.main()
