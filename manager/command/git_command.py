from manager.argument.arguments import Arguments
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command import Command
from manager.service.git.git_service import GitService


class GitArguments(Arguments):
    clone: bool = False

    def __init__(self, builder: CommandArgumentsBuilderInterface):
        builder.add_object(self)
        builder.add_bool_argument("--clone", "-cl", "Clone repositories")


class GitCommand(Command):
    """Runs git commands"""

    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        GitArguments(arguments_builder)

    def run(self, args: GitArguments):
        service = GitService(self.get_context().get_executor(), self.get_context().get_config().services_path)
        if args.clone:
            service.clone_repositories()
