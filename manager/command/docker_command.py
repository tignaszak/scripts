from manager.argument.arguments import Arguments
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command import Command
from manager.service.docker.docker_service import DockerService
from manager.service.file.env_service import EnvService


class DockerArguments(Arguments):
    clear: bool = False

    def __init__(self, builder: CommandArgumentsBuilderInterface):
        builder.add_object(self)
        builder.add_bool_argument("--clear", "-c", "Clear docker and creates init manager network")


class DockerCommand(Command):
    """Runs docker commands"""

    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        DockerArguments(arguments_builder)

    def run(self, args: DockerArguments):
        service = DockerService(self.get_context().get_executor())

        if args.clear:
            ports = EnvService(self.get_context().get_config().env_file_path, args.env).get_ports_tuple()
            service.clear(ports)
