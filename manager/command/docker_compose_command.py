from manager.argument.arguments import Arguments
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command import Command
from manager.service.docker.command_builder import CommandBuilder
from manager.service.docker.docker_compose_service import DockerComposeService
from manager.service.file.env_service import EnvService


class DockerComposeArguments(Arguments):
    docker_compose: str = ""
    command: str = ""
    resolve: bool = False

    def __init__(self, builder: CommandArgumentsBuilderInterface):
        builder.add_object(self)
        builder.add_optional_str_argument('--docker-compose', '-dc',
                                          "Path to docker-compose.yml file (same path by default)")
        builder.add_optional_str_argument('--command', '-cmd', "Specifies command to run, default 'up --build'")
        builder.add_bool_argument("--resolve", "-r",
                                  "Create 'docker-compose-resolved.yaml' with resolved environment variables")


class DockerComposeCommand(Command):
    """Runs docker-compose"""

    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        DockerComposeArguments(arguments_builder)

    def run(self, args: DockerComposeArguments):
        env_string = EnvService(self.get_context().get_config().env_file_path, args.env).get_env_vars_string()

        service = DockerComposeService(
            self.get_context().get_executor(),
            CommandBuilder(args.docker_compose, env_string),
            self.get_context().get_project_repository()
        )

        if args.resolve:
            service.resolve_config()
        else:
            service.exec(args.command)
