from manager.argument.arguments import Arguments
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command import Command
from manager.service.project.project_service import ProjectService


class ProjectArguments(Arguments):
    test: bool = False
    build: bool = False

    def __init__(self, builder: CommandArgumentsBuilderInterface):
        builder.add_object(self)
        builder.add_bool_argument("--test", "-t", "Run tests for all projects")
        builder.add_bool_argument("--build", "-b", "Build all projects")


class ProjectCommand(Command):
    """Runs project commands"""

    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        ProjectArguments(arguments_builder)

    def run(self, args: ProjectArguments):
        service = ProjectService(self.get_context().get_executor())
        if args.test:
            service.test(self.get_context().get_project_repository().find_all_projects())
        if args.build:
            service.build(self.get_context().get_project_repository().find_all_projects(), args.skip_tests)
