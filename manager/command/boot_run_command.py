from manager.argument.arguments import Arguments
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command import Command
from manager.service.facade.boot_run_facade import BootRunFacade
from manager.service.file.env_service import EnvService

BLUE = '\033[0;34m'


class BootRunArguments(Arguments):
    def __init__(self, builder: CommandArgumentsBuilderInterface):
        builder.add_object(self)


def print_swagger_uri(gateway_uri):
    print('\nPlease wait for services to complete registering and then open swagger: {}{}/swagger-ui.html\n'
          .format(BLUE, gateway_uri))


class BootRunCommand(Command):
    """Clone, build and run services"""

    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        BootRunArguments(arguments_builder)

    def run(self, args: BootRunArguments):
        env_service = EnvService(self.get_context().get_config().env_file_path, args.env)
        facade = BootRunFacade(
            self.get_context().get_executor(),
            self.get_context().get_project_repository(),
            env_service,
            self.get_context().get_config().services_path
        )
        facade.clone_repositories()
        print()
        facade.update_repos()
        print()
        facade.build_projects(args.skip_tests)
        print()
        facade.run_services()
        gateway_uri = env_service.get_env_var_by_name('manager_gateway_external_uri')
        print_swagger_uri(gateway_uri)
