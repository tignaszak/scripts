from manager.service.executor import Executor
from manager.service.file.config import Config
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface


class CommandContext(object):

    def __init__(self, config: Config, executor: Executor, project_repository: ProjectRepositoryInterface):
        self.__config = config
        self.__executor = executor
        self.__project_repository = project_repository

    def get_config(self) -> Config:
        return self.__config

    def get_executor(self) -> Executor:
        return self.__executor

    def get_project_repository(self) -> ProjectRepositoryInterface:
        return self.__project_repository
