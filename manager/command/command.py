from abc import ABC, abstractmethod
from typing import final

from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.command.command_context import CommandContext
from manager.command.command_exception import CommandException


class Command(ABC):

    __context: CommandContext

    @final
    def init_with_context(self, context: CommandContext, args):
        self.__context = context
        self.run(args)

    @abstractmethod
    def configure(self, arguments_builder: CommandArgumentsBuilderInterface):
        pass

    @abstractmethod
    def run(self, args):
        pass

    def get_context(self) -> CommandContext:
        if self.__context is None:
            raise CommandException("Command context is not set!")

        return self.__context
