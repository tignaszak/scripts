class Arguments:
    profile: str = ""
    debug: bool = False
    no_debug: bool = False
    background: bool = False
    no_background: bool = False
    service: str = ""
    env: str = ""
    skip_tests: bool = False
