from abc import ABC, abstractmethod


class ArgumentsBuilderInterface(ABC):

    @abstractmethod
    def add_option(self, name: str, description: str):
        pass

    @abstractmethod
    def build(self, option: str):
        pass

    @abstractmethod
    def start_option(self, option: str):
        pass
