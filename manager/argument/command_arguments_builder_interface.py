from abc import ABC, abstractmethod


class CommandArgumentsBuilderInterface(ABC):

    @abstractmethod
    def add_object(self, args_object: object):
        pass

    @abstractmethod
    def add_str_argument(self, name: str, short: str, description: str):
        pass

    @abstractmethod
    def add_optional_str_argument(self, name: str, short: str, description: str):
        pass

    @abstractmethod
    def add_bool_argument(self, name: str, short: str, description: str):
        pass
