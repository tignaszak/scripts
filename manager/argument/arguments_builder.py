import argparse
import re
from typing import Dict, List

from manager.argument.arguments import Arguments
from manager.argument.arguments_builder_interface import ArgumentsBuilderInterface
from manager.argument.command_arguments_builder_interface import CommandArgumentsBuilderInterface
from manager.manager_exception import ManagerException


class ArgumentsBuilder(ArgumentsBuilderInterface, CommandArgumentsBuilderInterface):

    arguments_to_option: Dict[str, List[str]] = {}
    option_to_object: Dict = {}
    options: List[str] = []
    active_option: str

    def __init__(self, parser: argparse.ArgumentParser):
        self.parser = parser
        self.init_arguments()

    def add_option(self, name: str, description: str):
        if name in self.options:
            raise ManagerException("Option '" + name + "' already exists!")
        else:
            self.options.append(name)
            self.parser.add_argument(name, action='store', help=description, nargs='?')

    def build(self, option: str):
        args = self.parser.parse_args()
        args_object = self.option_to_object[option]

        for attr in dir(args_object):
            if not attr.startswith("__"):
                setattr(args_object, attr, getattr(args, attr))

        return args_object

    def start_option(self, option: str):
        self.active_option = option

    def add_str_argument(self, name: str, short: str, description: str):
        self.add_argument(name)
        self.parser.add_argument(name, short, help=description, type=str)

    def add_optional_str_argument(self, name: str, short: str, description: str):
        self.add_argument(name)
        self.parser.add_argument(name, short, help=description, type=str, required=False)

    def add_bool_argument(self, name: str, short: str, description: str):
        self.add_argument(name)
        self.parser.add_argument(name, short, action='store_true', help=description)

    def init_arguments(self):
        self.start_option("manager_command_starter")
        self.add_object(Arguments())

        self.add_argument("--profile")
        self.parser.add_argument('--profile', '-p', help="Define working profile.", type=str, required=False,
                                 default='DEV')

        self.add_bool_argument('--debug', '-d', "Debug mode")
        self.add_bool_argument('--no-debug', '-nd', "Disable debug mode")
        self.add_bool_argument('--background', '-bg', "Run command in background")
        self.add_bool_argument('--no-background', '-nbg', "Don't run command in background")
        self.add_str_argument('--env', '-e', "Path to .env file")
        self.add_bool_argument("--skip-tests", "-st", "Skip tests")

        self.add_argument("--service")
        self.parser.add_argument('--service', '-s', help="Service name where command should be executed.", type=str,
                                 required=False, default='')

    def add_object(self, args_object):
        if args_object in self.arguments_to_option:
            raise ManagerException("Arguments object '" + type(args_object).__name__ + "' already added!")
        else:
            self.option_to_object[self.active_option] = args_object
            self.arguments_to_option[self.active_option] = []

    def add_argument(self, argument: str):
        if not re.compile("^--[a-zA-Z0-9_-]").match(argument):
            raise ManagerException("Invalid argument '" + argument + "'!")

        argument = argument.replace(r'^--', '')
        argument = argument.replace('-', '_')

        if not self.active_option:
            raise ManagerException("Command block is not started!")
        elif self.active_option not in self.arguments_to_option:
            raise ManagerException("Command '" + self.active_option + "' not exists in cache!")
        elif argument in self.arguments_to_option[self.active_option]:
            raise ManagerException("Argument '" + argument + "' already exists for command '"
                                   + self.active_option + "'!")
        else:
            self.arguments_to_option[self.active_option].append(argument)
