from manager.service.docker.command_builder import CommandBuilder
from manager.service.docker.docker_compose_service import DockerComposeService
from manager.service.executor import Executor
from manager.service.file.env_service import EnvService
from manager.service.git.git_service import GitService
from manager.service.project.project_service import ProjectService
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface


class BootRunFacade(object):

    def __init__(self,
                 executor: Executor,
                 project_repository: ProjectRepositoryInterface,
                 env_service: EnvService,
                 projects_path: str):
        self.__project_repository = project_repository
        self.__projects_path = projects_path
        self.__git_service = GitService(executor, projects_path)
        self.__project_service = ProjectService(executor)
        self.__env_service = env_service
        self.__docker_compose_service = DockerComposeService(
            executor,
            CommandBuilder('', self.__env_service.get_env_vars_string()),
            project_repository
        )

    def clone_repositories(self):
        self.__git_service.clone_repositories()

    def update_repos(self):
        self.__project_service.update_repos(self.__projects_path)

    def build_projects(self, skip_tests: bool):
        self.__project_service.build(self.__project_repository.find_all_projects(), skip_tests)

    def run_services(self):
        self.__docker_compose_service.exec('')
