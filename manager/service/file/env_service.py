import re
from typing import Tuple, List, Optional

from manager.manager_exception import ManagerException


def get_env_file_path(config_env_file_path: str, args_env_file_path: str) -> str:
    path = args_env_file_path

    if not args_env_file_path:
        if not config_env_file_path:
            raise ManagerException("Env file path is not set in manager conf.ini!")
        else:
            path = config_env_file_path

    return path


def read_env(path: str) -> str:
    with open(path, 'r') as file:
        envs = file.read()

        # Removes comments (only # in new lines!!) and empty lines
        envs = re.sub(r"(^\s*#.*)|(^\s*$)", '', envs, flags=re.MULTILINE)
        envs = re.sub(r"\n+", ' ', envs, flags=re.MULTILINE)

        return envs


class EnvService(object):

    def __init__(self, config_env_file_path: str, args_env_file_path: str):
        self.__path = get_env_file_path(config_env_file_path, args_env_file_path)
        self.__env_string = read_env(self.__path)

    def get_env_vars_string(self) -> str:
        return self.__env_string

    def get_env_var_by_name(self, name: str) -> Optional[str]:
        matches = re.finditer(re.escape(name) + r"=(?P<value>[^\s]+)", self.__env_string)
        for match in matches:
            return match.group('value')
        return None

    def get_ports_tuple(self) -> Tuple[str]:
        result: List[str] = []
        env_string = self.get_env_vars_string()

        if env_string:
            matches = re.finditer(r"_port\s*=\s*(?P<port>\d+)", env_string, flags=re.MULTILINE)
            for match in matches:
                port = match.group("port")
                result.append(port)

        return tuple(result)
