import configparser
import os.path
import shutil
from configparser import ConfigParser
from os import path

from manager.manager_exception import ManagerException
from manager.service.file.config import Config
from manager.service.utils import to_bool

DOCKER_COMPOSE_ENV_FILE = 'DockerComposeEnvFile'
EXECUTOR_BACKGROUND_ENABLED = 'ExecutorBackgroundEnabled'
DEBUG = 'Debug'
SERVICES_PATH = 'ServicesPath'

CONF_FILE = 'conf.ini'
CONF_INI_EXAMPLE = 'conf.ini.example'


def dict_to_config(config_dict) -> Config:
    config = Config()
    config.env_file_path = get_or_raise(config_dict, DOCKER_COMPOSE_ENV_FILE,
                                        "Could not read '" + DOCKER_COMPOSE_ENV_FILE + "' from conf.ini!")
    config.executor_background = to_bool(get_or_default(config_dict, EXECUTOR_BACKGROUND_ENABLED, False))
    config.debug = to_bool(get_or_default(config_dict, DEBUG, False))
    config.services_path = get_or_default(config_dict, SERVICES_PATH, '')
    return config


def get_or_default(config_dict, key: str, default):
    if key in config_dict:
        value = config_dict[key]
    else:
        value = default

    return value


def get_or_raise(config_dict, key: str, message: str):
    if key in config_dict:
        return config_dict[key]
    else:
        raise ManagerException(message)


class ConfigService(object):

    def __init__(self, root_path: str):
        self.__conf_ini_file = path.join(root_path, CONF_FILE)
        self.__init_config(root_path)
        self.__config_parser = ConfigParser()
        self.__config_parser.read(self.__conf_ini_file)
        self.__ini_file_path = root_path

    def get_config(self, section: str) -> Config:
        if section in self.__config_parser:
            return dict_to_config(self.__config_parser[section])
        else:
            raise ManagerException('Profile ' + section + ' does not exists in ' + self.__ini_file_path)

    def __init_config(self, root_path: str):
        if not os.path.isfile(self.__conf_ini_file):
            shutil.copy(path.join(root_path, CONF_INI_EXAMPLE), self.__conf_ini_file)
            config = configparser.RawConfigParser()
            config.read(self.__conf_ini_file)
            manager_path = path.dirname(root_path)
            main_dev_path = path.join(manager_path, 'Configuration', 'docker', 'main-dev.env')
            config.set('DEV', DOCKER_COMPOSE_ENV_FILE, main_dev_path)
            config.set('DEV', SERVICES_PATH, manager_path)
            cfg_file = open(self.__conf_ini_file, 'w')
            config.write(cfg_file, space_around_delimiters=False)
            cfg_file.close()
