class Config:
    env_file_path: str
    executor_background: bool
    debug: bool
    services_path: str
