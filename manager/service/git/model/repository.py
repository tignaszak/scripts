class Repository(object):

    def __init__(self, name: str, clone_link: str):
        self.__name = name
        self.__clone_link = clone_link

    def get_name(self) -> str:
        return self.__name

    def get_clone_link(self) -> str:
        return self.__clone_link
