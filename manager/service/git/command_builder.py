from os import path
from typing import List

from manager.service.git.model.repository import Repository


class CommandBuilder(object):

    def __init__(self, project_path: str):
        self.__project_path = project_path

    def build_clone_command(self, repo: Repository) -> List[str]:
        clone_path = path.join(self.__project_path, repo.get_name())
        return ['git', 'clone', repo.get_clone_link(), clone_path]
