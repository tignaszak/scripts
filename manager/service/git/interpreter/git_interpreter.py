from manager.service.git.interpreter.result.clone_result import CloneResult


class GitInterpreter(object):

    def interpret_clone_output(self, output: str) -> CloneResult:
        if output == 'skipped':
            return CloneResult(True, True)
        else:
            return CloneResult(not output.__contains__('fatal'))
