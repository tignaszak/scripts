from enum import Enum


class CloneStatus(Enum):
    SUCCESS = 1
    SKIPPED = 2
    FAILED = 3


class CloneResult(object):

    def __init__(self, is_success: bool, is_skipped=False):
        if is_skipped:
            self.status = CloneStatus.SKIPPED
        else:
            self.status = CloneStatus.SUCCESS if is_success else CloneStatus.FAILED
