import os

from manager.service.executor import Executor
from manager.service.git.client.git_client_factory import GitClientFactory, ClientType
from manager.service.git.command_builder import CommandBuilder
from manager.service.git.interpreter.git_interpreter import GitInterpreter
from manager.service.git.report.clone_repository_report_printer import CloneRepositoryReportPrinter


def get_directories(path: str):
    return next(os.walk(path))[1]


class GitService(object):

    def __init__(self, executor: Executor, projects_path: str):
        self.__executor = executor
        self.__projects_path = projects_path
        self.__command_builder = CommandBuilder(projects_path)
        self.__client = GitClientFactory(ClientType.BITBUCKET).create()
        self.__clone_report_printer = CloneRepositoryReportPrinter()
        self.__git_interpreter = GitInterpreter()

    def clone_repositories(self):
        self.__clone_report_printer.start_report()
        for repo in self.__client.get_repositories():
            self.__clone_report_printer.start_report_line(repo.get_name())
            output = self.__execute(repo)
            result = self.__git_interpreter.interpret_clone_output(output)
            self.__clone_report_printer.fill_report_line(repo.get_name(), result)
        self.__clone_report_printer.end_report()

    def __execute(self, repo) -> str:
        for project_dir in get_directories(self.__projects_path):
            if project_dir.lower() == repo.get_name().lower():
                return 'skipped'
        return self.__executor.check_exec(self.__command_builder.build_clone_command(repo))
