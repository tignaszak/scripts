from manager.service.git.interpreter.result.clone_result import CloneResult, CloneStatus
from manager.service.report.report_printer import ReportPrinter, GREEN, RED, YELLOW
from manager.service.report.report_printer_interface import ReportPrinterInterface


class CloneRepositoryReportPrinter(ReportPrinterInterface):

    def __init__(self):
        self.__report_printer = ReportPrinter()

    def start_report(self):
        self.__report_printer.start_report('Start cloning repositories:')

    def start_report_line(self, name: str):
        self.__report_printer.start_report_line("Start cloning {}".format(name))

    def end_report(self):
        self.__report_printer.end_report()

    def fill_report_line(self, name: str, result: CloneResult):
        match result.status:
            case CloneStatus.SUCCESS:
                color = GREEN
            case CloneStatus.SKIPPED:
                color = YELLOW
            case CloneStatus.FAILED:
                color = RED
            case _:
                color = ''

        self.__report_printer.fill_report_line(name, result.status.name, color)
