from abc import ABC, abstractmethod
from typing import Tuple

from manager.service.git.model.repository import Repository


class GitClientInterface(ABC):

    @abstractmethod
    def get_repositories(self) -> Tuple[Repository]:
        pass
