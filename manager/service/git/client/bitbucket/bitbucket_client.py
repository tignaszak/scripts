import json
from typing import Tuple

from manager.manager_exception import ManagerException
from manager.service.git.client.git_client_interface import GitClientInterface
from manager.service.git.client.http_client import HttpClient
from manager.service.git.model.repository import Repository

REPOSITORIES_ENDPOINT = '/2.0/repositories/tignaszak?q=project.key=%22MAN%22&fields=values.name,values.links.clone'


def parse_response(json_object) -> Tuple[Repository]:
    result = list()
    for repo in json_object['values']:
        for link in repo['links']['clone']:
            if link['name'] == 'https':
                result.append(Repository(repo['name'], link['href']))
    return tuple(result)


class BitbucketClient(GitClientInterface):

    BITBUCKET_HOST = 'api.bitbucket.org'

    def __init__(self, http_client: HttpClient):
        self.__http_client = http_client

    def get_repositories(self) -> Tuple[Repository]:
        response = self.__http_client.get(REPOSITORIES_ENDPOINT)
        if response.code == 200:
            json_object = json.loads(response.body)
            return parse_response(json_object)
        else:
            raise ManagerException("An error occur while calling '{}', code {}, message: {}".format(
                REPOSITORIES_ENDPOINT, str(response.code), response.body))
