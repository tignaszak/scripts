from enum import Enum

from manager.manager_exception import ManagerException
from manager.service.git.client.bitbucket.bitbucket_client import BitbucketClient
from manager.service.git.client.git_client_interface import GitClientInterface
from manager.service.git.client.http_client import HttpClient


class ClientType(Enum):
    BITBUCKET = 1


def build_bitbucket_client():
    return BitbucketClient(HttpClient(BitbucketClient.BITBUCKET_HOST))


class GitClientFactory(object):

    def __init__(self, client_type: ClientType):
        self.__client_type = client_type

    def create(self) -> GitClientInterface:
        match self.__client_type:
            case ClientType.BITBUCKET:
                return build_bitbucket_client()
            case _:
                raise ManagerException('No client is specified!')
