from http.client import HTTPSConnection


class Response(object):
    def __init__(self, code: int, body: str):
        self.code = code
        self.body = body


class HttpClient(object):

    def __init__(self, host: str):
        self.__connection = HTTPSConnection(host)

    def __del__(self):
        self.__connection.close()

    def get(self, path: str) -> Response:
        self.__connection.request('GET', path)
        response = self.__connection.getresponse()
        return Response(response.getcode(), response.read().decode())
