from os import path
from os.path import exists
from typing import Optional

from manager.service.project.model.gradle_project import GradleProject
from manager.service.project.model.maven_project import MavenProject
from manager.service.project.model.project import Project
from manager.service.service.model.service import Service
from manager.service.utils import get_scripts_name


def is_maven(project_path):
    return exists(path.join(project_path, 'pom.xml'))


def is_gradle(project_path):
    return exists(path.join(project_path, 'build.gradle.kts')) or exists(path.join(project_path, 'build.gradle'))


class ProjectFactory(object):

    def __init__(self, project_dir: str, projects_path: str, service: Optional[Service]):
        self.__project_dir = project_dir
        self.__projects_path = projects_path
        self.__service = service

    def build_if_exists(self) -> Optional[Project]:
        wrapper_path = path.join(self.__projects_path, get_scripts_name(self.__projects_path), 'bin', 'wrapper')
        project_path = path.join(self.__projects_path, self.__project_dir)

        return is_maven(project_path)\
            and MavenProject(self.__service, self.__project_dir, wrapper_path, project_path)\
            or (
                is_gradle(project_path)
                and GradleProject(self.__service, self.__project_dir, wrapper_path, project_path)
                or None
            )
