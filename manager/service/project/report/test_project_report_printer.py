from manager.service.project.interpreter.result.test_result import TestResult, TestStatus
from manager.service.report.report_printer_interface import ReportPrinterInterface
from manager.service.report.report_printer import ReportPrinter, GREEN, RED, YELLOW


class TestReportPrinterInterface(ReportPrinterInterface):
    def __init__(self):
        self.__report_printer = ReportPrinter()

    def start_report(self):
        self.__report_printer.start_report('Start testing projects:')

    def start_report_line(self, name: str):
        self.__report_printer.start_report_line("Start testing {}".format(name))

    def end_report(self):
        self.__report_printer.end_report()

    def fill_report_line(self, name: str, result: TestResult):
        match result.get_status():
            case TestStatus.SUCCESS:
                color = GREEN
            case TestStatus.FAILED:
                color = RED
            case TestStatus.NO_TESTS:
                color = YELLOW
            case _:
                color = ''

        self.__report_printer.fill_report_line(
            name,
            "{:10s} tests: {}, success: {}, failures: {}, skipped: {}".format(
                result.get_status().name, result.total_tests, result.success, result.failures, result.skipped),
            color
        )