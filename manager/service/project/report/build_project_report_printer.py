from manager.service.project.interpreter.result.build_result import BuildResult, BuildStatus
from manager.service.report.report_printer_interface import ReportPrinterInterface
from manager.service.report.report_printer import ReportPrinter, GREEN, RED


class BuildReportPrinterInterface(ReportPrinterInterface):
    def __init__(self):
        self.__report_printer = ReportPrinter()

    def start_report(self):
        self.__report_printer.start_report('Start building projects:')

    def start_report_line(self, name: str):
        self.__report_printer.start_report_line("Start building {}".format(name))

    def end_report(self):
        self.__report_printer.end_report()

    def fill_report_line(self, name: str, result: BuildResult):
        match result.status:
            case BuildStatus.SUCCESS:
                color = GREEN
            case BuildStatus.FAILED:
                color = RED
            case _:
                color = ''

        self.__report_printer.fill_report_line(name, result.status.name, color)
