from manager.manager_exception import ManagerException
from manager.service.project.interpreter.gradle_interpreter import GradleInterpreter
from manager.service.project.interpreter.interpreter_interface import InterpreterInterface
from manager.service.project.interpreter.maven_interpreter import MavenInterpreter
from manager.service.project.model.project import Project, ProjectType


class InterpreterFactory(object):

    def __init__(self, project: Project):
        self.__project = project

    def build_interpreter(self) -> InterpreterInterface:
        match self.__project.get_type():
            case ProjectType.MAVEN: return MavenInterpreter()
            case ProjectType.GRADLE: return GradleInterpreter()
            case _:
                raise ManagerException('Could not create interpreter for {} project type'
                                       .format(self.__project.get_type()))
