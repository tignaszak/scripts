from abc import ABC, abstractmethod

from manager.service.project.interpreter.result.build_result import BuildResult
from manager.service.project.interpreter.result.test_result import TestResult


class InterpreterInterface(ABC):

    @abstractmethod
    def interpret_test_output(self, output: str) -> TestResult:
        pass

    @abstractmethod
    def interpret_build_output(self, output: str) -> BuildResult:
        pass
