import re

from manager.service.project.interpreter.interpreter_interface import InterpreterInterface
from manager.service.project.interpreter.result.build_result import BuildResult
from manager.service.project.interpreter.result.test_result import TestResult


TEST_REGEX = r"Tests run: (?P<total>\d+), Failures: (?P<failures>\d+), Errors: (?P<errors>\d+), " \
        r"Skipped: (?P<skipped>\d+)(\n|\\n)"


class MavenInterpreter(InterpreterInterface):

    def interpret_test_output(self, output: str) -> TestResult:
        matches = re.finditer(TEST_REGEX, output, re.MULTILINE)
        result = TestResult()

        for match_num, match in reversed(list(enumerate(matches, start=1))):
            result.total_tests = result.total_tests + int(match.group('total'))
            result.failures = result.failures + int(match.group('failures')) + int(match.group('errors'))
            result.skipped = result.skipped + int(match.group('skipped'))
            break

        result.success = result.total_tests - result.failures - result.skipped

        return result

    def interpret_build_output(self, output: str) -> BuildResult:
        is_success = output.__contains__('BUILD SUCCESS')
        return BuildResult(is_success)
