from enum import Enum


class BuildStatus(Enum):
    SUCCESS = 1
    FAILED = 2


class BuildResult(object):

    def __init__(self, is_success: bool):
        self.status = BuildStatus.SUCCESS if is_success else BuildStatus.FAILED
