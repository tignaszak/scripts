from enum import Enum


class TestStatus(Enum):
    SUCCESS = 1
    FAILED = 2
    NO_TESTS = 3


class TestResult(object):

    def __init__(self):
        self.total_tests = 0
        self.success = 0
        self.failures = 0
        self.skipped = 0

    def get_status(self):
        match self.total_tests:
            case 0: return TestStatus.NO_TESTS
            case self.success: return TestStatus.SUCCESS
            case _: return TestStatus.FAILED

