import re

from manager.service.project.interpreter.interpreter_interface import InterpreterInterface
from manager.service.project.interpreter.result.build_result import BuildResult
from manager.service.project.interpreter.result.test_result import TestResult

TEST_REGEX = r"Gradle Test Executor \d+ results: (FAILURE|SUCCESS) \((?P<total>\d+) tests, " \
        r"(?P<successes>\d+) successes, (?P<failures>\d+) failures, (?P<skipped>\d+) skipped\)"


class GradleInterpreter(InterpreterInterface):

    def interpret_test_output(self, output: str) -> TestResult:
        matches = re.finditer(TEST_REGEX, output, re.MULTILINE)
        result = TestResult()

        for match_num, match in enumerate(matches, start=1):
            result.total_tests = result.total_tests + int(match.group('total'))
            result.success = result.success + int(match.group('successes'))
            result.failures = result.failures + int(match.group('failures'))
            result.skipped = result.skipped + int(match.group('skipped'))

        return result

    def interpret_build_output(self, output: str) -> BuildResult:
        is_success = output.__contains__('BUILD SUCCESSFUL')
        return BuildResult(is_success)
