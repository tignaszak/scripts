from os import path
from typing import Optional

from manager.service.project.model.project import Project, ProjectType
from manager.service.service.model.service import Service


class MavenProject(Project):

    def __init__(self, service: Optional[Service], name: str, wrapper_path: str, project_path: str):
        super().__init__(service, name)
        pom_path = path.join(project_path, 'pom.xml')
        self.__mvnw_project_pom = 'cd {}; ./mvnw -f {}'.format(wrapper_path, pom_path)

    def get_type(self) -> ProjectType:
        return ProjectType.MAVEN

    def get_test_command(self) -> str:
        return self.__mvnw_project_pom + ' clean test'

    def get_build_command(self, skip_tests: bool = False) -> str:
        skip_tests_flag = ' -DskipTests' if skip_tests else ''
        return self.__mvnw_project_pom + ' clean package' + skip_tests_flag
