from os import path
from os.path import exists
from typing import Optional

from manager.service.project.model.project import Project, ProjectType
from manager.service.service.model.service import Service


class GradleProject(Project):

    def __init__(self, service: Optional[Service], name: str, wrapper_path: str, project_path: str):
        super().__init__(service, name)
        build_file_name = exists(path.join(project_path, 'build.gradle.kts')) and 'build.gradle.kts' or 'build.gradle'
        build_file_path = path.join(project_path, build_file_name)
        self.__gradlew_project_build = 'cd {}; ./gradlew -b {}'.format(wrapper_path, build_file_path)

    def get_type(self) -> ProjectType:
        return ProjectType.GRADLE

    def get_test_command(self) -> str:
        return self.__gradlew_project_build + ' clean test'

    def get_build_command(self, skip_tests: bool = False) -> str:
        skip_tests_flag = ' -x test' if skip_tests else ''
        return self.__gradlew_project_build + ' clean build' + skip_tests_flag
