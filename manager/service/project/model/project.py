from abc import ABC, abstractmethod
from enum import Enum
from typing import Optional

from manager.service.service.model.service import Service


class ProjectType(Enum):
    MAVEN = 1
    GRADLE = 2


class Project(ABC):

    def __init__(self, service: Optional[Service], name: str):
        self.__service = service
        self.__name = name

    def __str__(self):
        return self.__name

    def get_service(self) -> Service:
        return self.__service

    def get_name(self) -> str:
        return self.__name

    @abstractmethod
    def get_type(self) -> ProjectType:
        pass

    @abstractmethod
    def get_test_command(self) -> str:
        pass

    @abstractmethod
    def get_build_command(self, skip_tests: bool = False) -> str:
        pass
