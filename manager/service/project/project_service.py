from os import path
from typing import Tuple, List

from manager.service.executor import Executor
from manager.service.project.interpreter.gradle_interpreter import GradleInterpreter
from manager.service.project.interpreter.interpreter_factory import InterpreterFactory
from manager.service.project.interpreter.interpreter_interface import InterpreterInterface
from manager.service.project.model.project import Project
from manager.service.project.report.build_project_report_printer import BuildReportPrinterInterface
from manager.service.report.report_printer_interface import ReportPrinterInterface
from manager.service.project.report.test_project_report_printer import TestReportPrinterInterface
from manager.service.utils import get_scripts_name


def get_interpreter(project: Project) -> InterpreterInterface:
    interpreter_factory = InterpreterFactory(project)
    interpreter = interpreter_factory.build_interpreter()
    return interpreter


class ProjectService(object):

    def __init__(self, executor: Executor):
        self.__executor = executor
        self.__test_report_printer: ReportPrinterInterface = TestReportPrinterInterface()
        self.__build_report_printer: ReportPrinterInterface = BuildReportPrinterInterface()

    def test(self, projects: Tuple[Project]):
        self.__test_report_printer.start_report()
        for project in projects:
            self.__test_report_printer.start_report_line(project.get_name())
            command = project.get_test_command()
            output = self.__executor.check_exec_shell(command)
            interpreter = get_interpreter(project)
            result = interpreter.interpret_test_output(output)
            self.__test_report_printer.fill_report_line(project.get_name(), result)
        self.__test_report_printer.end_report()

    def build(self, projects: Tuple[Project], skip_tests: bool):
        self.__build_report_printer.start_report()
        for project in projects:
            self.__build_report_printer.start_report_line(project.get_name())
            command = project.get_build_command(skip_tests)
            output = self.__executor.check_exec_shell(command)
            interpreter = get_interpreter(project)
            result = interpreter.interpret_build_output(output)
            self.__build_report_printer.fill_report_line(project.get_name(), result)
        self.__build_report_printer.end_report()

    def update_repos(self, projects_path: str):
        self.__build_report_printer.start_report()
        self.__build_report_printer.start_report_line('update repos')
        output = self.__executor.check_exec(self.__build_update_repo_command(projects_path))
        interpreter = GradleInterpreter()
        result = interpreter.interpret_build_output(output)
        self.__build_report_printer.fill_report_line('Update repos', result)
        self.__build_report_printer.end_report()

    def __build_update_repo_command(self, projects_path) -> List[str]:
        update_script_path = path.join(projects_path, get_scripts_name(projects_path), 'bin', 'manager-update')
        return ['sh', update_script_path]
