from abc import ABC, abstractmethod
from typing import Tuple

from manager.service.service.model.service import Service


class DependencyFacadeInterface(ABC):
    @abstractmethod
    def resolve(self) -> Tuple[Tuple[Service]]:
        pass
