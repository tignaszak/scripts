from abc import ABC, abstractmethod

from manager.service.service.model.service import Service


class ReaderInterface(ABC):
    @abstractmethod
    def read(self, service: Service):
        pass
