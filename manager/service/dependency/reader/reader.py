from os.path import exists
from typing import Set

import yaml

from manager.service.dependency.reader.reader_interface import ReaderInterface
from manager.service.dependency.repository.dependency_repository_interface import DependencyRepositoryInterface
from manager.service.service.model.service import Service
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface

DEPENDENCIES = 'dependencies'
OPTIONAL = 'optional'
REQUIRED = 'required'

SERVICE_BUILD_YML = "/service-build.yml"


class Reader(ReaderInterface):

    def __init__(self,
                 dependency_repository: DependencyRepositoryInterface,
                 service_repository: ProjectRepositoryInterface):
        self.__dependency_repository = dependency_repository
        self.__service_repository = service_repository

    def read(self, service: Service):
        build_file_path = service.get_path() + SERVICE_BUILD_YML
        if not self.__dependency_repository.contains_service(service):
            if not exists(build_file_path):
                self.__dependency_repository.add(service, set(), set())
            else:
                self.__read_from_yaml(build_file_path, service)

    def __read_from_yaml(self, build_file_path, service):
        with open(build_file_path) as stream:
            yaml_dict = yaml.safe_load(stream)
            if yaml_dict is not None and DEPENDENCIES in yaml_dict:
                dependencies = yaml_dict[DEPENDENCIES]
                required = self.__build_required_services_set(dependencies)
                optional = self.__build_optional_services_set(dependencies)
                self.__dependency_repository.add(service, required, optional)
                for sub_service in set.union(required, optional):
                    self.read(sub_service)

    def __build_optional_services_set(self, dependencies) -> Set[Service]:
        optional = set()
        if OPTIONAL in dependencies:
            optional = self.__build_services_set(dependencies[OPTIONAL])
        return optional

    def __build_required_services_set(self, dependencies) -> Set[Service]:
        required = set()
        if REQUIRED in dependencies:
            required = self.__build_services_set(dependencies[REQUIRED])
        return required

    def __build_services_set(self, dependencies: Set[str]) -> Set[Service]:
        result = set()
        for dependency in dependencies:
            service = self.__service_repository.find_service_by_name(dependency)
            if service is not None:
                result.add(service)
        return result
