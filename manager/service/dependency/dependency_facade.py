from typing import Tuple

from manager.service.dependency.dependency_facade_interface import DependencyFacadeInterface
from manager.service.dependency.reader.reader import Reader
from manager.service.dependency.repository.dependency_repository import DependencyRepository
from manager.service.dependency.resolver.resolver import Resolver
from manager.service.service.model.service import Service
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface


class DependencyFacade(DependencyFacadeInterface):

    def __init__(self, service_repository: ProjectRepositoryInterface):
        self.__service_repository = service_repository
        self.__dependency_repository = DependencyRepository()
        self.__fill_dependency_repository()

    def resolve(self) -> Tuple[Tuple[Service]]:
        return Resolver(self.__dependency_repository)\
            .resolve(self.__service_repository.find_selected_services())

    def __fill_dependency_repository(self):
        reader = Reader(self.__dependency_repository, self.__service_repository)
        for service in self.__service_repository.find_all_services():
            reader.read(service)
