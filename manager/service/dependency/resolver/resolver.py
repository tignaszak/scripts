from typing import Set, Dict, Tuple

from manager.service.dependency.repository.dependency_repository_interface import DependencyRepositoryInterface
from manager.service.dependency.resolver.resolver_interface import ResolverInterface
from manager.service.service.model.service import Service


def add_to_nodes(dependencies: Set[Service], nodes: Set[Service]):
    for dependency in dependencies:
        nodes.add(dependency)


def add_required(nodes, optional_set: Set[Service], services: Tuple[Service], result):
    nodes = sorted(nodes, reverse=True)
    services_dict: Dict[int, Set[str]] = {}

    for node in nodes:
        if node.count > 0 and node.service not in optional_set and node.service not in services:
            if node.count not in services_dict:
                services_dict[node.count] = set()
            services_dict[node.count].add(node.service)

    for key in services_dict:
        result.append(tuple(sorted(services_dict[key])))


def add_optional(optional_set: Set[Service], services: Tuple[Service], result):
    optional_to_add = []

    for optional in optional_set:
        if optional not in services:
            optional_to_add.append(optional)

    if len(optional_to_add) > 0:
        result.append(tuple(sorted(optional_to_add)))


class Resolver(ResolverInterface):

    class Node:
        service: Service
        count: int

        def __init__(self, service: Service):
            self.service = service
            self.count = 0

        def __lt__(self, other):
            return self.count < other.count

        def __gt__(self, other):
            return self.count > other.count

        def increase(self):
            self.count += 1

    def __init__(self, repository: DependencyRepositoryInterface):
        self.__repository = repository
        self.__done: Set[Service] = set()

    def resolve(self, services: Tuple[Service]) -> Tuple[Tuple[Service], ...]:
        result = []

        required_set: Set[Service] = set()
        optional_set: Set[Service] = set()

        for service in services:
            if self.__repository.contains_service(service):
                self.build_requires_set(service, required_set)
                self.build_optional_set(service, optional_set)

        nodes = self.build_node_set()
        for service in optional_set | required_set:
            self.count_nodes(service, nodes)

        add_required(nodes, optional_set, services, result)
        add_optional(optional_set, services, result)

        return tuple(result)

    def count_nodes(self, service: Service, nodes: Set[Node]):
        for node in nodes:
            if service == node.service:
                node.increase()
        if self.__repository.contains_service(service) and service not in self.__done:
            dependency = self.__repository.find_dependency_by_service(service)
            for parent in dependency.optional | dependency.required:
                if parent not in dependency.optional:
                    self.__done.add(parent)
                self.count_nodes(parent, nodes)

    def build_node_set(self) -> Set[Node]:
        services: Set[Service] = set()
        for service in self.__repository.find_service_to_dependency_map():
            dependency = self.__repository.find_dependency_by_service(service)
            add_to_nodes(dependency.required, services)
            add_to_nodes(dependency.optional, services)
            services.add(service)
        nodes = set()
        for node in services:
            nodes.add(self.Node(node))
        return nodes

    def build_optional_set(self, service: Service, dep_set: Set[Service]):
        if self.__repository.contains_service(service):
            dependency = self.__repository.find_dependency_by_service(service)
            for optional_service in dependency.optional:
                dep_set.add(optional_service)
                self.build_optional_set(optional_service, dep_set)

    def build_requires_set(self, service: Service, dep_set: Set[Service]):
        if self.__repository.contains_service(service):
            dependency = self.__repository.find_dependency_by_service(service)
            for required_service in dependency.required:
                dep_set.add(required_service)
                self.build_requires_set(required_service, dep_set)
