from abc import ABC, abstractmethod
from typing import Tuple

from manager.service.service.model.service import Service


class ResolverInterface(ABC):
    @abstractmethod
    def resolve(self, services: Tuple[Service]) -> Tuple[Tuple[Service]]:
        pass
