from abc import ABC, abstractmethod
from typing import Set, Dict

from manager.service.dependency.repository.dependency import Dependency
from manager.service.service.model.service import Service


class DependencyRepositoryInterface(ABC):
    @abstractmethod
    def add(self, service: Service, required: Set[Service], optional=None):
        pass

    @abstractmethod
    def find_service_to_dependency_map(self) -> Dict[Service, Dependency]:
        pass

    @abstractmethod
    def find_dependency_by_service(self, service: Service) -> Dependency:
        pass

    @abstractmethod
    def contains_service(self, service: Service) -> bool:
        pass
