from abc import ABC
from dataclasses import dataclass
from typing import Set

from manager.service.service.model.service import Service


@dataclass
class Dependency(ABC):
    required: Set[Service]
    optional: Set[Service]
