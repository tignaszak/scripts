from typing import Set, Dict

from manager.service.dependency.repository.dependency import Dependency
from manager.service.dependency.repository.dependency_repository_interface import DependencyRepositoryInterface
from manager.service.service.model.service import Service


class DependencyRepository(DependencyRepositoryInterface):

    __dependencies: Dict[Service, Dependency] = {}

    def add(self, service: Service, required: Set[Service], optional=None):
        if optional is None:
            optional = set()
        self.__dependencies[service] = Dependency(required, optional)
        return self

    def find_service_to_dependency_map(self) -> Dict[Service, Dependency]:
        return self.__dependencies

    def find_dependency_by_service(self, service: Service) -> Dependency:
        return self.__dependencies[service]

    def contains_service(self, service: Service) -> bool:
        return service in self.__dependencies
