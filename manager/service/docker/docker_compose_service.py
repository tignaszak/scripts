import time

from manager.service.dependency.dependency_facade import DependencyFacade
from manager.service.docker.command_builder import UP_BUILD_COMMAND
from manager.service.docker.command_builder_interface import CommandBuilderInterface
from manager.service.docker.image.repository.image_repository import ImageRepository
from manager.service.executor import Executor
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface

NETWORK = 'manager-network'


class DockerComposeService(object):

    def __init__(self,
                 executor: Executor,
                 command_builder: CommandBuilderInterface,
                 service_repository: ProjectRepositoryInterface):
        self.__executor = executor
        self.__command_builder = command_builder
        self.__service_repository = service_repository
        self.__dependency_facade = DependencyFacade(service_repository)
        self.__image_repository = ImageRepository(executor)

    def exec(self, command: str = UP_BUILD_COMMAND):
        self.__check_network()
        self.__start_dependencies(command)
        self.__start_services(command)

    def resolve_config(self):
        for service in self.__service_repository.find_selected_services():
            resolve_command = self.__command_builder.docker_compose_resolve_config_command(service)
            self.__executor.check_exec_shell(resolve_command)

    def __check_network(self):
        print('Check network...')
        command = self.__command_builder.find_network_command(NETWORK)
        output = self.__executor.check_exec_shell(command)
        if not output.__contains__(NETWORK):
            command = self.__command_builder.create_network_command(NETWORK)
            self.__executor.check_exec_shell(command)

    def __start_services(self, command):
        service_names = []
        for service in self.__service_repository.find_selected_services():
            service_names.append(service.get_name())
            self.__remove_service_image(service)
            service_command = self.__command_builder.docker_compose_up_command(service, command)
            self.__executor.exec_shell(service_command)
        if service_names:
            print('Starting services ({})...'.format(', '.join(service_names)))

    def __remove_service_image(self, service):
        image = self.__image_repository.find_image_by_name(service.get_image_name())
        if image is not None:
            service_command = self.__command_builder.create_remove_container_command(service)
            self.__executor.check_exec_shell(service_command)
            image_command = self.__command_builder.create_remove_image_command(image)
            self.__executor.check_exec_shell(image_command)

    def __start_dependencies(self, command):
        for services in self.__dependency_facade.resolve():
            sleep = False
            service_names = []
            for service in services:
                with self.__executor.exec_output_shell(self.__command_builder.container_status_command(service)) as p:
                    for line in p.stdout:
                        if line.find('running') < 0:
                            service_names.append(service.get_name())
                            command_shell = self.__command_builder.docker_compose_up_command(service, command)
                            self.__executor.exec_silent_shell(command_shell)
                            sleep = True
                            break
            if service_names:
                print('Starting dependencies ({})...'.format(', '.join(service_names)))
            if sleep:
                time.sleep(20)  # It is enough for now
