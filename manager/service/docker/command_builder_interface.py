from abc import ABC, abstractmethod

from manager.service.docker.image.model.image import Image
from manager.service.service.model.service import Service


class CommandBuilderInterface(ABC):

    @abstractmethod
    def docker_compose_up_command(self, service: Service, command: str) -> str:
        pass

    @abstractmethod
    def docker_compose_resolve_config_command(self, service: Service) -> str:
        pass

    @abstractmethod
    def container_status_command(self, service: Service) -> str:
        pass

    @abstractmethod
    def find_network_command(self, network: str) -> str:
        pass

    @abstractmethod
    def create_network_command(self, network: str) -> str:
        pass

    @abstractmethod
    def create_remove_image_command(self, image: Image) -> str:
        pass

    @abstractmethod
    def create_remove_container_command(self, service: Service) -> str:
        pass
