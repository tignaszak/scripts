from manager.service.docker.command_builder_interface import CommandBuilderInterface
from manager.service.docker.image.model.image import Image
from manager.service.service.model.service import Service

UP_BUILD_COMMAND = 'up'


def get_docker_compose_shell(docker_compose: str, service_path: str) -> str:
    if docker_compose:
        command = '-f ' + docker_compose
    elif service_path:
        command = '-f {}/docker-compose.yml'.format(service_path)
    else:
        command = ''
    return command


def get_command_flag_shell(command: str) -> str:
    if not command:
        command = UP_BUILD_COMMAND
    return command


class CommandBuilder(CommandBuilderInterface):

    def __init__(self, docker_compose: str, env_string: str):
        self.__docker_compose = docker_compose
        self.__env_string = env_string

    def docker_compose_up_command(self, service: Service, command: str) -> str:
        docker_compose_shell = get_docker_compose_shell(self.__docker_compose, service.get_path())
        return '{} docker-compose {} {}'.format(self.__env_string, docker_compose_shell, get_command_flag_shell(command))

    def docker_compose_resolve_config_command(self, service: Service) -> str:
        return '{} docker-compose -f {}/docker-compose.yml config > {}/docker-compose-resolved.yml'.format(
            self.__env_string,
            service.get_path(),
            service.get_path()
        )

    def container_status_command(self, service: Service) -> str:
        return "docker container inspect -f '{{.State.Status}}' " + service.get_container_name()

    def find_network_command(self, network: str) -> str:
        return 'docker network ls | grep ' + network

    def create_network_command(self, network: str) -> str:
        return 'docker network create ' + network

    def create_remove_image_command(self, image: Image) -> str:
        return 'docker image rm {} -f'.format(image.get_id())

    def create_remove_container_command(self, service: Service) -> str:
        return 'docker container rm {} -f'.format(service.get_container_name())
