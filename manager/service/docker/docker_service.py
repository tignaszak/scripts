from typing import Tuple

from manager.service.executor import Executor


class DockerService(object):

    def __init__(self, executor: Executor):
        self.executor = executor

    def clear(self, ports: Tuple[str]):
        command = "docker system prune -f"\
                  + " && service postgresql stop"\
                  + " && service postgresql start"\
                  + " && docker network create manager-network || true"

        for port in ports:
            command += " && kill -9 `sudo lsof -t -i:" + port + "`"

        self.executor.exec_shell(command)
