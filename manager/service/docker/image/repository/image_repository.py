import re
from typing import Optional

from manager.service.docker.image.model.image import Image
from manager.service.docker.image.repository.image_repository_interface import ImageRepositoryInterface
from manager.service.executor import Executor


class ImageRepository(ImageRepositoryInterface):

    def __init__(self, executor: Executor):
        self.__executor = executor

    def find_image_by_name(self, name: str) -> Optional[Image]:
        output = self.__executor.check_exec_shell('docker image ls | grep ' + name)
        matches = re.finditer(re.escape(name) + r"\s+\w+\s+([a-z0-9]+)", output)
        for match_num, match in enumerate(matches, start=1):
            return Image(name, match.group(1))
        return None
