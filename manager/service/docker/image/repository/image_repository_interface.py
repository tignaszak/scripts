from abc import ABC, abstractmethod
from typing import Optional

from manager.service.docker.image.model.image import Image


class ImageRepositoryInterface(ABC):

    @abstractmethod
    def find_image_by_name(self, name: str) -> Optional[Image]:
        pass
