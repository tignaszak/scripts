class Image(object):

    def __init__(self, image_name: str, image_id: str):
        self.__name = image_name
        self.__id = image_id

    def get_name(self) -> str:
        return self.__name

    def get_id(self) -> str:
        return self.__id
