from os import path

from manager.manager_exception import ManagerException


def get_decision(conf, args, no_args) -> bool:
    if no_args:
        decision = False
    elif args:
        decision = True
    else:
        decision = to_bool(conf)
    return decision


def to_bool(value) -> bool:
    if type(value) == str:
        result = value == 'True' or value == 'true'
    elif type(value) == bool:
        result = value
    else:
        raise ManagerException("could not convert '" + value + "' to bool!")
    return result


def get_scripts_name(projects_path: str) -> str:
    return path.isdir(path.join(projects_path, 'Scripts')) and 'Scripts' or 'scripts'
