import subprocess
from typing import List

from manager.argument.arguments import Arguments
from manager.service.file.config import Config
from manager.service.utils import get_decision

BACKGROUND_OPTION = ' > /dev/null 2>&1 &'


class Executor(object):
    def __init__(self, background: bool, debug: bool):
        self.__background = background
        self.__debug = debug

    def exec_shell(self, command: str) -> None:
        self.__debug_command(command)
        stdout = self.__background and BACKGROUND_OPTION or ''
        subprocess.Popen(command + stdout, shell=True)

    def check_exec(self, command: List[str]) -> str:
        self.__debug_command(' '.join(command))
        try:
            return str(subprocess.check_output(command, stderr=subprocess.PIPE))
        except subprocess.CalledProcessError as e:
            return str(e.stderr)

    def check_exec_shell(self, command: str) -> str:
        self.__debug_command(command)
        try:
            return str(subprocess.check_output(command, shell=True, stderr=subprocess.PIPE))
        except subprocess.CalledProcessError as e:
            return str(e.output)

    def exec_output_shell(self, command: str) -> subprocess.Popen:
        return subprocess.Popen(
            command,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )

    def exec_silent_shell(self, command: str) -> None:
        subprocess.Popen(command + BACKGROUND_OPTION, shell=True)

    def __debug_command(self, command):
        if self.__debug:
            print('---COMMAND---\n' + command + '\n-------------')

    class Builder(object):

        config: Config
        arguments: Arguments

        def set_config(self, config: Config):
            self.config = config
            return self

        def set_arguments(self, arguments: Arguments):
            self.arguments = arguments
            return self

        def build(self, force_background: bool = False):
            if force_background:
                background = True
            else:
                background = get_decision(self.config.executor_background, self.arguments.background,
                                          self.arguments.no_background)

            debug = get_decision(self.config.debug, self.arguments.debug, self.arguments.no_debug)

            return Executor(background, debug)
