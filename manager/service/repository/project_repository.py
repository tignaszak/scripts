from typing import Tuple

from manager.manager_exception import ManagerException
from manager.service.project.model.project import Project
from manager.service.repository.project_repository_interface import ProjectRepositoryInterface
from manager.service.service.model.service import Service


class ProjectRepository(ProjectRepositoryInterface):

    def __init__(self, projects: Tuple[Project]):
        if projects:
            self.__projects = projects
        else:
            self.__projects = tuple()

    def find_all_projects(self) -> Tuple[Project]:
        return tuple(self.__projects)

    def find_all_services(self) -> Tuple[Service]:
        result = []
        for project in self.__projects:
            if project.get_service() is not None:
                result.append(project.get_service())
        return tuple(result)

    def find_selected_services(self) -> Tuple[Service]:
        result = []
        for service in self.find_all_services():
            if service.is_selected():
                result.append(service)
        return tuple(sorted(result))

    def find_service_by_name(self, name: str) -> Service:
        for service in self.find_all_services():
            if name == service.get_name():
                return service

        raise ManagerException("Service '{}' not found!".format(name))
