from abc import ABC, abstractmethod
from typing import Tuple

from manager.service.project.model.project import Project
from manager.service.service.model.service import Service


class ProjectRepositoryInterface(ABC):

    @abstractmethod
    def find_all_projects(self) -> Tuple[Project]:
        pass

    @abstractmethod
    def find_all_services(self) -> Tuple[Service]:
        pass

    @abstractmethod
    def find_selected_services(self) -> Tuple[Service]:
        pass

    @abstractmethod
    def find_service_by_name(self, name: str) -> Service:
        pass
