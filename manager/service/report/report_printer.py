YELLOW = '\033[0;33m'
RED = '\033[0;31m'
GREEN = '\033[0;32m'

GET_BACK = '\033[F'
CLEAR = '\033[0m'


class ReportPrinter(object):

    __index: int = 1

    def start_report(self, value: str):
        print(value)

    def start_report_line(self, value: str):
        print(" {:2d}. {}...".format(self.__index, value))

    def end_report(self):
        self.__index = 1

    def fill_report_line(self, name: str, value: str, color: str):
        print("{} {:2d}. {:20s}    {}{}{}".format(GET_BACK, self.__index, name, color, value, CLEAR))

        self.__index = self.__index + 1
