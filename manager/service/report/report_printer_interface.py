from abc import abstractmethod, ABC


class ReportPrinterInterface(ABC):

    @abstractmethod
    def start_report(self):
        pass

    @abstractmethod
    def start_report_line(self, name: str):
        pass

    @abstractmethod
    def end_report(self):
        pass

    @abstractmethod
    def fill_report_line(self, name: str, result):
        pass
