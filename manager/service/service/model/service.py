from abc import ABC
from re import sub


class Service(ABC):
    def __init__(self, name: str, path: str, container_name: str, is_selected: bool = False):
        self.__name = name
        self.__path = path
        self.__container_name = container_name
        self.__is_selected = is_selected

    def __str__(self):
        return self.__name

    def __lt__(self, other):
        return self.__name < other.get_name()

    def __gt__(self, other):
        return self.__name > other.get_name()

    def get_name(self) -> str:
        return self.__name

    def get_path(self) -> str:
        return self.__path + "/" + self.__name

    def get_container_name(self) -> str:
        return self.__container_name

    def get_image_name(self) -> str:
        return self.__container_name

    def is_selected(self) -> bool:
        return self.__is_selected

    def get_main_class_name(self) -> str:
        part = sub(r"(_|-)+", " ", self.get_name()).title().replace(" ", "")
        return ''.join([part[0], part[1:]]) + 'Application'
