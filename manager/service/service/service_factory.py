import re
from os import path
from os.path import exists
from typing import List, Optional

import yaml

from manager.manager_exception import ManagerException
from manager.service.service.model.service import Service

SERVICE_PATTERN = re.compile(r"^[a-zA-Z0-9-]+$")


class ServiceFactory(object):

    def __init__(self, service_dir: str, services_path: str, services: List[str]):
        self.__service_dir = service_dir
        self.__services_path = services_path
        self.__services = services

    def build_if_exists(self) -> Optional[Service]:
        docker_compose_path = path.join(self.__services_path, self.__service_dir, 'docker-compose.yml')
        if SERVICE_PATTERN.search(self.__service_dir) and exists(docker_compose_path):
            return Service(
                self.__service_dir,
                self.__services_path,
                self.__get_container_name(docker_compose_path),
                self.__is_selected()
            )
        else:
            return None

    def __get_container_name(self, docker_compose_path) -> Optional[str]:
        matches = re.finditer(r"^(V\d+)-([a-zA-Z]+)", self.__service_dir)
        version = ''
        name = ''
        regex = r"^manager-"
        for match in matches:
            version = match.group(1).lower()
            name = match.group(2).lower()
        if version:
            regex = regex + name + "-" + version + "-service"
        else:
            regex = regex + self.__service_dir.lower()
        with open(docker_compose_path) as stream:
            yaml_dict = yaml.safe_load(stream)
            if 'services' in yaml_dict:
                for dc_service in yaml_dict['services']:
                    if re.compile(regex).search(dc_service):
                        return dc_service
        raise ManagerException('Could not find container name for service: ' + self.__service_dir)

    def __is_selected(self) -> bool:
        result: bool = False
        for serv in self.__services:
            if re.compile(r"^" + serv + "$").search(self.__service_dir):
                result = True
        return result
