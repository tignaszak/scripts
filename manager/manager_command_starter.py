import os
import sys

from typing import Dict, Tuple, Set

from manager.argument.arguments_builder import ArgumentsBuilder
from manager.command.command import Command
from manager.command.command_context import CommandContext
from manager.manager_exception import ManagerException
from manager.service.executor import Executor
from manager.service.file.config_service import ConfigService
from manager.service.project.model.project import Project
from manager.service.project.project_factory import ProjectFactory

from manager.service.repository.project_repository import ProjectRepository
from manager.service.service.service_factory import ServiceFactory


def build_project_tuple(projects_path: str, service: str) -> Tuple[Project]:
    result: Set[Project] = set()

    for project_dir in get_directories(projects_path):
        selected_services = service and service.split(',') or list()
        service_factory = ServiceFactory(project_dir, projects_path, selected_services)

        project_factory = ProjectFactory(project_dir, projects_path, service_factory.build_if_exists())
        project = project_factory.build_if_exists()
        if project is not None:
            result.add(project)

    return tuple(result)


def get_directories(path: str):
    return next(os.walk(path))[1]


class ManagerCommandStarter(object):

    __commands: Dict[str, Command] = {}

    def __init__(self,
                 arguments_builder: ArgumentsBuilder,
                 config_service: ConfigService,
                 executor_builder: Executor.Builder):
        self.__arguments_builder = arguments_builder
        self.__config_service = config_service
        self.__executor_builder = executor_builder

    def add(self, option: str, command: Command):
        if option in self.__commands:
            raise ManagerException("Command already exists: " + option)
        self.__arguments_builder.add_option(option, command.__doc__)
        self.__commands[option] = command

    def init(self):
        for option in self.__commands:
            self.__arguments_builder.start_option(option)
            self.__commands[option].configure(self.__arguments_builder)

        if len(sys.argv) > 1:
            option = sys.argv[1]
            if option in self.__commands:
                args_object = self.__arguments_builder.build(option)
                context = self.__build_command_context(args_object)
                self.__commands[option].init_with_context(context, args_object)

    def __build_command_context(self, args_object):
        config = self.__config_service.get_config(args_object.profile)
        project_tuple = build_project_tuple(config.services_path, args_object.service)
        project_repository = ProjectRepository(project_tuple)
        executor = self.__executor_builder \
            .set_config(config) \
            .set_arguments(args_object) \
            .build()

        return CommandContext(config, executor, project_repository)
