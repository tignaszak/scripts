#!/usr/bin/env python3

import argparse
import os

from manager.argument.arguments_builder import ArgumentsBuilder
from manager.command.boot_run_command import BootRunCommand
from manager.command.docker_command import DockerCommand
from manager.command.docker_compose_command import DockerComposeCommand
from manager.command.git_command import GitCommand
from manager.command.project_command import ProjectCommand
from manager.manager_command_starter import ManagerCommandStarter
from manager.service.executor import Executor
from manager.service.file.config_service import ConfigService


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    config_service = ConfigService(dir_path)

    starter = ManagerCommandStarter(
        ArgumentsBuilder(argparse.ArgumentParser()),
        config_service,
        Executor.Builder()
    )

    starter.add('docker-compose', DockerComposeCommand())
    starter.add('docker', DockerCommand())
    starter.add('project', ProjectCommand())
    starter.add('git', GitCommand())
    starter.add('boot-run', BootRunCommand())

    starter.init()


if __name__ == '__main__':
    main()
